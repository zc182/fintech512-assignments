var symbolSelect = document.getElementById("symbol-select");

var ready = (callback) => {
    if (document.readyState != "loading") callback();
    else document.addEventListener("DOMContentLoaded", callback);
  }

  ready(() => {
     let submit = document.getElementById("submit-button");
     submit.addEventListener("click", doClick)

     symbolSelect.addEventListener("change",makePlot);
  });

function doClick() {alert("here")}

function makePlot() {
    var symbol = symbolSelect.value;
    console.log("makePlot: start for " + symbol);
    fetch("data/" + symbol + ".csv")
    .then((response) => response.text()) /* asynchronous */
    .catch(error => {
        alert(error);
    })
    .then((text) => {
        console.log("csv: start for " + symbol);
        csv().fromString(text).then(processData)  /* asynchronous */
        console.log("csv: end for " + symbol);
    })
    console.log("makePlot: end for " + symbol);
};

function processData(data) {
    console.log("processData: start")
    let x = [], y = []

    for (let i=0; i<data.length; i++) {
        row = data[i];
        x.push( row['Date'] );
        y.push( row['Close'] );
    }
    makePlotly( x, y );
    console.log("processData: end")
}

function makePlotly( x, y ){
    var symbol = symbolSelect.value;
    console.log("makePlotly: start")
    var traces = [{
        x: x,
        y: y
    }];
    var layout  = { title: symbol +" Stock Price History"}

    myDiv = document.getElementById('myDiv');
    Plotly.newPlot( myDiv, traces, layout );
    console.log("makePlotly: end")
};
