import base64
from io import BytesIO
import requests
from flask import Flask, render_template, request, url_for, redirect, json
from datetime import datetime, timedelta
from matplotlib.figure import Figure

app = Flask(__name__)


# Define a route for the homepage

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        symbol = request.form['symbol']
        return redirect(url_for('create', symbol=symbol))
    else:
        return render_template('index.html')


@app.route('/create/<symbol>', methods=['GET'])
def create(symbol):
    api = api_name()
    company_overview = get_company_overview(symbol, api_name())
    company_news = get_company_news(symbol, api_name())
    stock_info = get_stock_info(symbol, api_name())
    # js_data = create_json(stock_info)
    for key in stock_info['price_trend'].keys():
        stock_info['price_trend'][key] = float(stock_info['price_trend'][key])
    # print(stock_info['price_trend'])
    data = create_json(stock_info)

    return render_template('create.html', company_overview=company_overview, company_news=company_news, symbol=symbol,
                           stock_info=stock_info, data=data)


def api_name():
    api = 'XQA1PUCWVATFXW5N'
    return api


def get_company_overview(symbol, api):
    # company overview
    company_overview = {}
    url = 'https://www.alphavantage.co/query?function=OVERVIEW&symbol=' + symbol + '&apikey=' + api
    r = requests.get(url)
    data = r.json()
    for key in data.keys():
        if key not in company_overview.keys():
            if key == "Symbol" or key == "Name" or key == "Description" or key == "Exchange" or key == "Sector" or key == "Industry" or key == "MarketCapitalization" or key == "PERatio" or key == "DividendPerShare" or key == "DividendYield" or key == "52WeekHigh" or key == "52WeekLow":
                company_overview[key] = data[key]
    return company_overview


def get_company_news(symbol, api):
    url = 'https://www.alphavantage.co/query?function=NEWS_SENTIMENT&tickers=' + symbol + '&apikey=' + api
    r = requests.get(url)
    data = r.json()
    company_news = {}
    for i in range(5):
        company_news[i] = {
            'title': data['feed'][i]['title'],
            'summary': data['feed'][i]['summary'],
            'url': data['feed'][i]['url']
        }
    return company_news


def get_stock_info(symbol, api):
    url = 'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol=' + symbol + '&outputsize=full&' + '&apikey=' + api
    r = requests.get(url)
    data = r.json()
    date_history = sorted(data['Time Series (Daily)'], reverse=True)
    current_price = data['Time Series (Daily)'][date_history[0]]['4. close']
    previous_close = data['Time Series (Daily)'][date_history[1]]['4. close']
    previous_open = data['Time Series (Daily)'][date_history[1]]['1. open']
    previous_vol = data['Time Series (Daily)'][date_history[1]]['6. volume']
    date_str = date_history[0]
    date_obj = datetime.strptime(date_str, '%Y-%m-%d').date()
    last_year = (date_obj - timedelta(days=365)).strftime('%Y-%m-%d')
    price_trend = {}
    for date in date_history:
        price_trend[date] = data['Time Series (Daily)'][date]['4. close']
        if date == last_year:
            break
    stock_info = {
        'current_price': current_price,
        'previous_close': previous_close,
        'previous_open': previous_open,
        'previous_vol': previous_vol,
        'price_trend': price_trend
    }
    return stock_info


def create_json(stock_info):
    dates = list(stock_info['price_trend'].keys())
    prices = list(stock_info['price_trend'].values())
    for i in range(len(prices)):
        prices[i] = float(prices[i])
    dates = [datetime.strptime(date, '%Y-%m-%d') for date in dates]
    fig = Figure()
    ax = fig.subplots()
    ax.plot(dates, prices)
    # Save it to a temporary buffer.
    buf = BytesIO()
    fig.savefig(buf, format="png")
    # Embed the result in the html output.
    data = base64.b64encode(buf.getbuffer()).decode("ascii")
    return data

# f"<img src='data:image/png;base64,{data}'/>"


if __name__ == '__main__':
    app.run(debug=True)
