import requests
from datetime import datetime, timedelta

from matplotlib import pyplot as plt


def api_name():
    api = 'JN6NHTH7KGBJZHA9'
    return api


def sort2dic(symbol, api):
    # company overview
    company_overview = {}
    url = 'https://www.alphavantage.co/query?function=OVERVIEW&symbol=' + symbol + '&apikey=' + api
    r = requests.get(url)
    data = r.json()
    for key in data.keys():
        if key not in company_overview.keys():
            if key == "Symbol" or key == "Name" or key == "Description" or key == "Exchange" or key == "Sector" or key == "Industry" or key == "MarketCapitalization" or key == "PERatio" or key == "DividendPerShare" or key == "DividendYield" or key == "52WeekHigh" or key == "52WeekLow":
                company_overview[key] = data[key]
    # print(company_overview)
    # company_news
    company_news = {}
    url = 'https://www.alphavantage.co/query?function=NEWS_SENTIMENT&tickers=' + symbol + '&apikey=' + api
    r = requests.get(url)
    data = r.json()
    for i in range(5):
        company_news[i] = {
            'title': data['feed'][i]['title'],
            'summary': data['feed'][i]['summary'],
            'url': data['feed'][i]['url']
        }
    # for i in range(5):
    #     company_new[str(i)] = data['feed'][i]
    print(company_news)

    # stock value
    stock_value = {}
    url = 'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol=' + symbol + '&outputsize=full&' + '&apikey=' + api
    r = requests.get(url)
    data = r.json()
    date_history = sorted(data['Time Series (Daily)'], reverse=True)
    price_info = {}
    price_info['current_price'] = data['Time Series (Daily)'][date_history[0]]['4. close']
    price_info['previous_close'] = data['Time Series (Daily)'][date_history[1]]['4. close']
    price_info['previous_open'] = data['Time Series (Daily)'][date_history[1]]['1. open']
    price_info['previous_vol'] = data['Time Series (Daily)'][date_history[1]]['6. volume']
    print(price_info)

    date_str = date_history[0]
    date_obj = datetime.strptime(date_str, '%Y-%m-%d').date()  # 将日期字符串转换为datetime.date对象
    last_year = (date_obj - timedelta(days=365)).strftime('%Y-%m-%d')
    price_trend = {}
    for date in date_history:
        price = data['Time Series (Daily)'][date]['4. close']
        price_trend[date] = price
        if date == last_year:
            break
    print(price_trend)
    print(price_trend[0].key)
    print(price_trend[0].value)
    dates = list(price_trend.keys())
    prices = list(price_trend.values())
    dates = [datetime.strptime(date, '%Y-%m-%d') for date in dates]
    max_price = 0
    min_price = float(prices[0])
    for ele in prices:
        if float(ele) > max_price:
            max_price = int(float(ele) // 1) + 1
        if float(ele) < min_price:
            min_price = int(float(ele) // 1)

    plt.plot(dates, prices)
    plt.yticks(range(min_price, max_price + 10, 10))
    plt.xlabel('Date')
    plt.ylabel('Price')
    plt.show()
    return company_overview, company_news, price_info, price_trend


if __name__ == "__main__":
    api = api_name()
    symbol = "IBM"
    company_overview, company_news, price_info, price_trend = sort2dic(symbol, api)
