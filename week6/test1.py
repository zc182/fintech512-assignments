import os

# Get API key from environment variable
api_key = os.environ.get('API_KEY')

# Check if API key is not None
if api_key is not None:
    print("API key found:", api_key)
else:
    print("API key not found")