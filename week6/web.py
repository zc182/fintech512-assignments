import os
import sqlite3
from datetime import datetime
from sqlite3 import IntegrityError
import requests
from flask import Flask, request, render_template, redirect, url_for, session

app = Flask(__name__)
app.secret_key = "1020"


# Connect to the database


# Define the route for the registration page
@app.route('/register', methods=['GET', 'POST'])
def register():
    conn = sqlite3.connect('static/week6.db')
    c = conn.cursor()
    error_msg = None
    if request.method == 'POST':
        # Get the data from the HTML form
        userid = request.form['userid']
        username = request.form['username']
        password = request.form['password']
        print(userid, username, password)
        if not userid or not username or not password:
            error_msg = "Please fill in all required fields"
        # Insert the data into the user_info table
        else:
            try:
                c.execute("INSERT INTO User_Info (userid, username, userpassword) VALUES (?, ?, ?)",
                          (userid, username, password))
                conn.commit()
                conn.close()
                # Redirect to the login page on successful registration
                return redirect(url_for('login'))
            except IntegrityError:
                # Handle any errors that occur during database insertion
                error_msg = "User: " + userid + " is already registered."
    # Render the registration page with any error messages
    return render_template('register.html', error_msg=error_msg)


# Define a function to check if the user exists and the password is correct
def check_user(userid, password):
    conn = sqlite3.connect('static/week6.db')
    c = conn.cursor()
    # Check if the user exists
    c.execute("SELECT userpassword FROM user_info WHERE userid=?", (userid,))
    result = c.fetchone()
    if result is None:
        return "User does not exist"
    # print(result)
    # Check if the password is correct
    if result[0] != password:
        return "Incorrect password"
    # If everything checks out, return None (no error)
    return None


@app.route('/')
def index():
    return render_template('login.html')


@app.route("/", methods=("GET", "POST"))
def login():
    if request.method == "POST":
        userid = request.form['userid']
        password = request.form['password']
        # Check if the user exists and the password is correct
        error = check_user(userid, password)
        if error:
            return render_template('login.html', error=error)
        session['userid'] = userid
        # If no error, redirect to post.html
        return redirect(url_for('post'))


@app.route('/post', methods=("GET", "POST"))
def post():
    if request.method == "GET":
        userid = session.get('userid')
        conn = sqlite3.connect('static/week6.db')
        c = conn.cursor()
        c.execute("SELECT username FROM user_info WHERE userid=?", (userid,))
        username = c.fetchone()[0]
        c.execute('SELECT * FROM price_info WHERE userid=?', (userid,))
        rows = c.fetchall()
        return render_template('post.html', username=username, data=rows)
    elif request.method == "POST":
        conn = sqlite3.connect('static/week6.db')
        c = conn.cursor()
        symbol = request.form['symbol']
        userid = session.get('userid')
        submit = request.form['submit']
        if submit == "Delete":
            tracking_list = "stock"
            c.execute("DELETE FROM price_info WHERE userid=? AND tracking_list=? AND symbol=?",(userid, tracking_list, symbol))
            conn.commit()
            conn.close()
            return redirect(url_for('post'))
        elif submit == "Update":
            api = os.environ.get('API_KEY')
            url = 'https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=' + symbol + '&interval=5min&apikey=' + api
            r = requests.get(url)
            data = r.json()
            date_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            latest_prices = []
            for timestamp, values in data["Time Series (5min)"].items():
                latest_prices.append(float(values["4. close"]))
            latest_prices = latest_prices[:2]  # 获取最新的两个价格
            percentage_change = (latest_prices[0] / latest_prices[1] - 1) * 100
            current_price = latest_prices[0]
            tracking_list = "stock"
            query = "UPDATE price_info SET date_time=?, current_price=?, percentage_change=? WHERE userid=? AND tracking_list=? AND symbol=?"
            c.execute(query, (date_time, current_price, percentage_change, userid, tracking_list, symbol))
            conn.commit()
            conn.close()
            return redirect(url_for('post'))




@app.route('/search', methods=("GET", "POST"))
def search():
    api = os.environ.get('API_KEY')
    print(1)
    print(api)
    print(1)
    if request.method == "POST":
        symbol = request.form['symbol']
        tracking_price = request.form['tracking_price']
        volume = request.form['volume']
        # Set the corresponding API according to the value of the checkbox
        category = "stock"
        percentage_change = 0
        latest_prices = []
        if category == 'stock':
            url = 'https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=' + symbol + '&interval=5min&apikey=' + api
            r = requests.get(url)
            data = r.json()
            if 'Error Message' in data.keys():
                error = "Invalid Symbol, please retry"
                return render_template('search.html', error=error)
            # print(data)
            latest_prices = []
            for timestamp, values in data["Time Series (5min)"].items():
                latest_prices.append(float(values["4. close"]))
            latest_prices = latest_prices[:2]  # 获取最新的两个价格
            percentage_change = (latest_prices[0] / latest_prices[1] - 1) * 100
        userid = session.get('userid')
        tracking_list = category
        date_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        current_price = latest_prices[0]
        try:
            conn = sqlite3.connect('static/week6.db')
            c = conn.cursor()
            c.execute(
                "INSERT INTO price_info (userid, tracking_list, symbol, date_time, tracking_price, volume, current_price, "
                "percentage_change) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
                (userid, tracking_list, symbol.upper(), date_time, float(tracking_price), volume, current_price,
                 percentage_change))
            conn.commit()
            conn.close()
            error = symbol.upper() + " is added now"
            return render_template('search.html', error=error)
        except IntegrityError:
            error = "Please enter symbol(text),tracking_price(float),volume(int) correctly or symbol has been added"
            return render_template('search.html', error=error)
    return render_template('search.html')





if __name__ == '__main__':
    app.run(debug=True)
